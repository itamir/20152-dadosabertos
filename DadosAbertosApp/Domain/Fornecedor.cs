﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DadosAbertosApp.Domain
{
    public class Fornecedor
    {
        public string nome { get; set; }
        public string cpfCnpj { get; set; }
    }
}
